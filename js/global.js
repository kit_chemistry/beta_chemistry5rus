//SOUNDS
var audio = [];

//TIMEOUTS
var timeout = [];

function TypingText(jqueryElement, interval, hasSound, endFunction)
{
	this.text = jqueryElement.html();
	this.jqueryElement = jqueryElement;
	this.interval = interval;
	this.currentSymbol = 0;
	this.endFunction = endFunction;
	this.hasSound = hasSound;
	this.audio = new Audio("audio/keyboard-sound.mp3");
	this.audio.addEventListener("ended", function(){
			this.play();
		});
	
	this.timeout;
	this.jqueryElement.html("");
}

TypingText.prototype.write = function()
{
	tObj = this;
	if (tObj.audio.paused && tObj.hasSound) {
		tObj.audio.play();
	}
	tObj.timeout = setTimeout(function(){
		tObj.jqueryElement.append(tObj.text[tObj.currentSymbol]);
		tObj.currentSymbol ++;
		if (tObj.currentSymbol < tObj.text.length) 
			tObj.write();
		else
		{
			tObj.audio.pause();
			tObj.endFunction();
		}
	}, tObj.interval);
}

TypingText.prototype.stopWriting = function()
{
	clearTimeout(this.timeout);
	this.audio.pause();
}

var loadImages = function(){
	jQuery.get('fileNames.txt', function(data) {
		var imagesSrc = data.split("\n"),
			images = [],
			loadPercentage = 0,
			imagesNum = imagesSrc.length - 1;
			unitToAdd = Math.round(100 / imagesNum),
			preLoadImage = $("#pre-load .pre-load-image");
		
		var imageLoadListener = function(){
			loadPercentage += unitToAdd;
			preLoadImage.css("width", loadPercentage + "%");
			console.log(loadPercentage);
			imagesNum --;
			if (loadPercentage >= 100) {
				//hideEverythingBut($("#frame-000"));
			}
		};
		
		for (var i = 0; i < imagesSrc.length - 1; i ++)
		{
			images[i] = new Image();
			images[i].src = "pics/" + imagesSrc[i];
			images[i].addEventListener("load", imageLoadListener);
		}
	});
}

function DragTask(jqueryElements, successCondition, successFunction, failFunction, finishCondition, finishFunction)
{
	this.draggables = jqueryElements;
	this.draggabillies = [];
	this.vegetable;
	this.basket;
	
	this.makeThemDraggable = function()
	{
		for(var i = 0; i < this.draggables.length; i++)
			this.draggabillies[i] = new Draggabilly(this.draggables[i]);
	}
	
	this.addEventListeners = function()
	{
		for(var i = 0; i < this.draggabillies.length; i++)
		{
			this.draggabillies[i].on("dragStart", this.onStart);
			this.draggabillies[i].on("dragEnd", this.onEnd);
		}
	}
	
	this.onEnd = function(instance, event, pointer)
	{
		var currVeg = this.vegetable;
		var currBasket = this.basket;
		currVeg.fadeOut(0);
		currBasket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));
		currVeg.fadeIn(0);
		
		if (currBasket.attr("data-key") && successCondition(currVeg.attr("data-key"), currBasket.attr("data-key")))
				successFunction(currVeg, currBasket);
		else
			failFunction(currVeg, currBasket);
		
		currVeg.removeClass("box-shadow-white");
		currVeg.css("opacity", "");
		
		if (finishCondition())
		{
			finishFunction();
		}
	}
	
	this.onStart = function(instance, event, pointer)
	{
		this.vegetable = $(event.target);
		this.vegetable.css("z-index", "9999");
		this.vegetable.addClass("box-shadow-white");
		this.vegetable.css("opacity", "0.6");
	}
	
	this.makeThemDraggable();
	this.addEventListeners();
}

var blink = function(jqueryElements, interval, times)
{
	var intervalHalf = Math.round(interval/2);
	timeout[0] = setTimeout(function(){
		jqueryElements.css("outline", "3px solid red");
		timeout[1] = setTimeout(function(){
			jqueryElements.css("outline", "");
			if (times) 
				blink(jqueryElements, interval, --times)		
		}, intervalHalf);
	}, intervalHalf);
}

var launch000 = function()
{
	
}

var launch101 = function()
{
	var theFrame = $("#frame-101"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		crystal = $(prefix + ".s2-crystal-glow"),
		textContainer = $(prefix + ".text-container"),
		typingText = new TypingText(textContainer, 80, false, function(){
			blackSkin.fadeIn(0);
			nextButton.fadeIn(0);
		});
	
	audio[0] = new Audio("audio/s1-1.mp3");
		
	audio[0].addEventListener("ended", function(){
		audio[1].play();
	});
	
	var doneSecond = 0;
	
	audio[0].addEventListener("timeupdate", function(){
		currAudio = this;
		currTime = Math.round(currAudio.currentTime);
		
		if(currTime === 8 && currTime != doneSecond)
		{
			doneSecond = currTime;
			crystal.css("height", "96.4%");
		}
	});
		
	nextButton.fadeOut(0);
		
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		typingText.write();
		audio[0].play();
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch102 = function()
{
	var theFrame = $("#frame-102"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		ionicCrystal = $(prefix + ".s2-ionic-crystal"),
		overSaturatedSolution = $(prefix + ".s2-oversaturated-solution"),
		crystalization = $(prefix + ".s2-crystalization"),
		solubility = $(prefix + ".s2-solubility"),
		vocabulary = $(prefix + ".vocabulary");
	
	audio[0] = new Audio("audio/s2-1.mp3");
		
	vocabulary.fadeOut(0);
		
	audio[0].addEventListener("ended", function(){
		audio[1].play();
		solubility.fadeIn(500);
	});
	var doneSecond = 0;
	audio[0].addEventListener("timeupdate", function(){
		var currAudio = this,
			currTime = Math.round(currAudio.currentTime);
			
		if(currTime === 6 && currTime !== doneSecond)
		{
			doneSecond = currTime;
			solubility.fadeIn(500);
		}
		else if(currTime === 12 && currTime !== doneSecond)
		{
			doneSecond = currTime;
			solubility.fadeOut(0);
			overSaturatedSolution.fadeIn(500);
		}
		else if(currTime === 16 && currTime !== doneSecond)
		{
			doneSecond = currTime;
			overSaturatedSolution.fadeOut(0);
			ionicCrystal.fadeIn(500);
		}
		else if(currTime === 22 && currTime !== doneSecond)
		{
			doneSecond = currTime;
			ionicCrystal.fadeOut(0);
			crystalization.fadeIn(500);
		}
	});
	
	audio[0].addEventListener("ended", function(){
		setTimeout(function(){
			crystalization.fadeOut(500);
			blackSkin.fadeIn(0);
			nextButton.fadeIn(0);
		}, 3000);
	});
	
	nextButton.fadeOut(0);
		
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		audio[0].play();
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch103 = function()
{
	var theFrame = $("#frame-103"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		cuprumSulfade = $(prefix + ".s3-cuprum-sulfade"),
		nickelChloride = $(prefix + ".s3-nickel-chloride"),
		potassiumPermanganat = $(prefix + ".s3-potassium-permanganat"),
		instructions = $(prefix + ".instructions"),
		pics = $(prefix + ".pic");
	
	audio[0] = new Audio("audio/s3-1.mp3");
			
	audio[0].addEventListener("timeupdate", function(){
		var currAudio = this, 
			currTime = Math.round(currAudio.currentTime), 
			doneSecond = 0;
			
		if(currTime === 8 && currTime !== doneSecond)
		{
			instructions.fadeIn(500);
		}
	});
	audio[0].addEventListener("ended", function(){
		timeout[3] = setTimeout(function(){
			blackSkin.fadeIn(0);
			nextButton.fadeIn(0);
		}, 3000);
	});
	
	nextButton.fadeOut(0);
	pics.fadeOut(0);
		
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		audio[0].play();
		timeout[0] = setTimeout(function(){
			cuprumSulfade.fadeIn(500);
		}, 1000);
		timeout[1] = setTimeout(function(){
			nickelChloride.fadeIn(500);
		}, 2000);
		timeout[2] = setTimeout(function(){
			potassiumPermanganat.fadeIn(500);
		}, 3000);
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch104 = function()
{
	var theFrame = $("#frame-104"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		labels = $(prefix + ".label"),
		items = $(prefix + ".item"),
		glass = $(prefix + ".glass"),
		spoon = $(prefix + ".spoon"),
		glassMix = $(prefix + ".glass-mix"),
		glassPour = $(prefix + ".glass-pour"),
		petry = $(prefix + ".petry"),
		thermometer = $(prefix + ".thermometer"),
		saltCan = $(prefix + ".salt-can"),
		tasks = $(prefix + ".task"),
		task1 = $(prefix + ".task-1"),
		task2 = $(prefix + ".task-2"),
		task3 = $(prefix + ".task-3"),
		task4 = $(prefix + ".task-4"),
		spoonNum = 0;
	
	thermSprite = new Motio(thermometer[0], {
		"fps": "2",
		"frames": "4"
	});
	thermSprite.on("frame", function(){
		if (this.frame === this.frames - 1)
		{
			this.pause();
			timeout[0] = setTimeout(function(){
				blackSkin.fadeIn(0);
				nextButton.fadeIn(0);
				audio[3].play();
			}, 5000);
		}
	});
	
	glassSprite = new Motio(glass[0], {
		"fps": "2",
		"frames": "5"
	});
	glassSprite.to(3, true);
	
	glassMixSprite = new Motio(glassMix[0], {
		"fps": "3",
		"frames": "4"
	});
	
	spoonSprite = new Motio(spoon[0], {
		"fps": "2",
		"frames": "3"
	})
	
	petrySprite = new Motio(petry[0], {
		"fps": "3",
		"frames": "5"
	});
	
	glassPourSprite = new Motio(glassPour[0], {
		"fps": "2",
		"frames": "3"
	});
	
	glassPourSprite.on("frame", function(){
		if (this.frame === this.frames - 1) {
			this.pause();
			audio[2].pause();
			glassPour.fadeOut(0);
			glass.fadeIn(0);
			glass.css("left", "40%");
			var glassPos = glassSprite.frame;
			glassPos --;
			glassSprite.to(glassPos, true);
			if (!glassPos)
			{
				glass.fadeOut(0);
				tasks.fadeOut(0);
				task3.fadeIn(0);
				thermometer.fadeIn(0);
				
				var thermSuccessIf = function(vegetable, basket)
				{
					return basket === "petry";
				}
				
				thermSuccessFunc = function(vegetable, basket)
				{
					vegetable.css({
						"top": "35%",
						"left": "65%"
					});
					thermSprite.play();
				}
				
				var dragTask3 = new DragTask(thermometer, thermSuccessIf, thermSuccessFunc, function(){}, function(){}, function(){});
			}
		}
	});
	audio[0] = new Audio("audio/salt-sound.mp3");
	audio[1] = new Audio("audio/water-stir.mp3");
	audio[2] = new Audio("audio/water-pour.mp3");
	audio[3] = new Audio("audio/s4-1.mp3");
	
	glassMixSprite.on("frame", function(){
		if (this.frame === this.frames - 1) {
			this.pause();
			glassMix.fadeOut(0);
			glassSprite.to(3, true);
			glass.fadeIn(0);
			spoon.fadeIn(0);
			spoonSprite.to(0, true);
			audio[1].pause();
			audio[0].currentTime = 0;
		}
	});
	
	//DragTask(jqueryElements, successCondition, successFunction, failFunction, finishCondition, finishFunction)
	
	var successCondition = function(vegetable, basket)
	{
		return basket === "salt-can" || basket === "salt-can-2" || basket === "glass" || basket === "petry";
	}
	
	var successFunction = function(vegetable, basket)
	{
		if (basket.attr("data-key") === "salt-can")
		{
			audio[0].play();
			spoonSprite.to(2, true);
		}
		if (basket.attr("data-key") === "salt-can-2")
		{
			audio[0].play();
			spoonSprite.to(1, true);
		}
		if (basket.attr("data-key") === "glass" && spoonSprite.frame === 2)
		{
			audio[1].currentTime = 0;
			audio[1].play();
			vegetable.fadeOut(0);
			glass.fadeOut(0);
			glassMix.fadeIn(0);
			glassMixSprite.play();
			
			spoonNum ++;
			if (spoonNum === 4) {
				tasks.fadeOut(0);
				task2.fadeIn(500);
				glass.attr("data-key", "glass-no");
				saltCan.attr("data-key", "salt-can-2");
				petry.fadeIn(0);
			}
		}
		if (basket.attr("data-key") === "petry" && spoonSprite.frame === 1)
		{
			petrySprite.to(1, true);
			saltCan.fadeOut(0);
			spoon.fadeOut(0);
			
			var glassSuccessIf = function(vegetable, basket)
			{
				return basket === "petry";
			}
			
			var glassSuccessFunc = function(vegetable, basket)
			{
				var petryFrame = petrySprite.frame;
				petryFrame ++;
				glass.fadeOut(0);
				glassPour.fadeIn(0);
				audio[2].play();
				glassPourSprite.play();
				petrySprite.to(petryFrame, true);
				
				if (petryFrame === petrySprite.frames - 1) {
					glass.fadeOut(0);
				}
			}
			
			var dragTask2 = new DragTask(glass, glassSuccessIf, glassSuccessFunc, function(){}, function(){});
		}
	}
	
	var failFunction = function(vegetable, basket)
	{
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
	}
	
	var dragTask = new DragTask(spoon, successCondition, successFunction, failFunction, function(){}, function(){});
	
	nextButton.fadeOut(0);
	tasks.fadeOut(0);
	labels.fadeOut(0);
	glassMix.fadeOut(0);
	glassPour.fadeOut(0);
	thermometer.fadeOut(0);
	items.css({
		"background-color": "transparent",
		"border-color": "transparent"
	});
		
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		items.css({
			"background-color": "",
			"border-color": ""
		});
		labels.fadeIn(0);
		timeout[0] = setTimeout(function(){
			items.css({
				"background-color": "transparent",
				"border-color": "transparent"
			});
			labels.fadeOut(0);
			items.fadeOut(0);
			glass.fadeIn(0);
			spoon.fadeIn(0);
			saltCan.fadeIn(0);
			task1.fadeIn(500);
		}, 5000);
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch105 = function()
{
	var theFrame = $("#frame-105"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		draggables = $(prefix + ".ball"),
		award = $(prefix + ".award"),
		snowfall = $(prefix + ".snowfall"),
		saltContainer = $(".salt-container");
	
	audio[0] = new Audio("audio/award-sound.mp3");
	audio[1] = new Audio("audio/s5-1.mp3");
	
	var snowfallSprite = new Motio(snowfall[0], {
		fps: 3,
		frames: 3
	});
	
	award.fadeOut(0);
	snowfall.fadeOut(0);
	
	audio[1].addEventListener("ended", function(){
		timeout[0] = setTimeout(function(){
			snowfall.fadeOut(0);
			award.fadeOut(0);
			blackSkin.fadeIn(0);
			nextButton.fadeIn(0);
		}, 5000);
	});
	
	var successCondition = function(vegetableKey, basketKey)
	{
		return basketKey.indexOf(vegetableKey) >= 0;
	}
	
	var successFunction = function(vegetable, basket)
	{
		basket.html(vegetable.html());
		vegetable.remove();
	}
	
	var failFunction = function(vegetable, basket)
	{
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
		vegetable.removeClass("box-shadow-white");
		vegetable.css("opacity", "");
	}
	
	var finishCondition = function()
	{
		return !$(prefix + " .ball").length;
	}
	
	var finishFunction = function()
	{
		audio[0].play();
		award.fadeIn(1000);
		snowfall.fadeIn(1000);
		snowfallSprite.play();
		saltContainer.append("<div class='salt'></div>");
		timeout[1] = setTimeout(function(){
			audio[1].play();
		}, 1000);
	}
	
	var dragTask = new DragTask(draggables, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	nextButton.fadeOut(0);
		
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch106 = function()
{
	var theFrame = $("#frame-106"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		answers = $(prefix + ".answer"),
		checkButton = $(prefix + ".check-button"),
		answerfields = $(prefix + ".answerfield"),
		result = $(prefix + ".result"),
		award = $(prefix + ".award"),
		saltContainer = $(".salt-container"),
		snowfall = $(prefix + ".snowfall"),
		correctNum = 0;
	
	audio[0] = new Audio("audio/s4-1.mp3");
	audio[1] = new Audio("audio/award-sound.mp3");
	audio[2] = new Audio("audio/s6-1.mp3");
	
	snowfallSprite = new Motio(snowfall[0], {
		"fps": "3",
		"frames": "3"
	});
	
	nextButton.fadeOut(0);
	result.fadeOut(0);
	award.fadeOut(0);
	snowfall.fadeOut(0);
	answerfields.val("");
	checkButton.fadeOut(0);
		
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		audio[2].play();
	};
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
	
	var answerListener = function(){
		var answer = $(this);
		checkButton.fadeIn(0);
		answer.css("font-weight", "bold");
		answer.siblings().css("font-weight", "");
		
		if (answer.attr("data-correct"))
			correctNum ++;
	};
	answers.off("click", answerListener);
	answers.on("click", answerListener);
	
	var checkListener = function(){
		for(var i = 0; i < answerfields.length; i++)
		{
			console.log($(answerfields[i]).val());
			if ($(answerfields[i]).val() === "Арал" ||
				$(answerfields[i]).val() === "Каспий")
			{
				correctNum ++;
			}
		}
		
		blackSkin.fadeIn(0);
		result.append("<br>" + correctNum);
		result.fadeIn(0);
		if (correctNum >= 5)
		{
			timeout[0] = setTimeout(function(){
				audio[1].play();
				result.fadeOut(0);
				award.fadeIn(0);
				snowfall.fadeIn(0);
				snowfallSprite.play();
				saltContainer.append("<div class='salt'></div>");
			}, 3000);
		}
		
		timeout[1] = setTimeout(function(){
			result.fadeOut(0);
			award.fadeOut(0);
			snowfallSprite.pause();
			snowfall.fadeOut(0);
			nextButton.fadeIn(0);
		}, 5000);
	};
	checkButton.off("click", checkListener);
	checkButton.on("click", checkListener);
}

var launch107 = function()
{
	var theFrame = $("#frame-107"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		draggables = $(prefix + ".ball"),
		award = $(prefix + ".award"),
		snowfall = $(prefix + ".snowfall"),
		result = $(prefix + ".result"),
		correctNum = 0,
		checkButton = $(prefix + ".check-button"),
		answerfields = $(prefix + ".answerfield"),
		saltContainer = $(".salt-container");
	
	audio[0] = new Audio("audio/award-sound.mp3");
	audio[1] = new Audio("audio/s7-1.mp3");
	
	var snowfallSprite = new Motio(snowfall[0], {
		fps: 3,
		frames: 3
	});
	
	var successCondition = function(vegetableKey, basketKey){
		return vegetableKey === basketKey;
	};
	var successFunction = function(vegetable, basket){
		basket.html(vegetable.html());
		vegetable.remove();
		correctNum++;
	};
	var failFunction = function(vegetable, basket)
	{
		vegetable.fadeIn(0);
		vegetable.css("left", "");
		vegetable.css("top", "");
	}
	
	var finishCondition = function()
	{
		return !$(prefix + " .ball").length;
	}
	
	var finishFunction = function()
	{
		checkButton.fadeIn(0);
	}
	
	var dragTask = new DragTask(draggables, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	nextButton.fadeOut(0);
	award.fadeOut(0);
	snowfall.fadeOut(0);
	result.fadeOut(0);
	checkButton.fadeOut(0);
	
	answerfields.val("");
		
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		audio[1].play();
	};
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
	
	var checkListener = function(){
		for(var i = 0; i < answerfields.length; i++)
		{
			var min = parseInt($(answerfields[i]).attr("data-key-min"));
			var max = parseInt($(answerfields[i]).attr("data-key-max"));
			var answer = parseInt($(answerfields[i]).val());
			
			if ((answer >= min) && (answer <= max)) {
				correctNum ++;
			}
		}
		
		result.append("<br>" + correctNum);
		blackSkin.fadeIn(0);
		result.fadeIn(0);
		
		if (correctNum >= 15)
		{
			timeout[2] = setTimeout(function(){
				audio[0].play();
				result.fadeOut(0);
				award.fadeIn(1000);
				snowfall.fadeIn(1000);
				snowfallSprite.play();
				saltContainer.append("<div class='salt'></div>");
			}, 3000);
		}
		timeout[3] = setTimeout(function(){
			result.fadeOut(0);
			award.fadeOut(0);
			snowfallSprite.pause();
			snowfall.fadeOut(0);
			nextButton.fadeIn(0);
		}, 5000);
	}
	checkButton.off("click", checkListener);
	checkButton.on("click", checkListener);
}

var launch108 = function()
{
	var theFrame = $("#frame-108"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		answers = $(prefix + ".answer"),
		checkboxes = $(prefix + ".check"),
		checkButton = $(prefix + ".check-button"),
		checkAnswer = $(prefix + ".check-answer"),
		result = $(prefix + ".result"),
		award = $(prefix + ".award"),
		snowfall = $(prefix + ".snowfall"),
		correctNum = 0,
		saltContainer = $(".salt-container");
	
	var snowfallSprite = new Motio(snowfall[0], {
		fps: 3,
		frames: 3
	});
	
	audio[0] = new Audio("audio/s8-1.mp3");
	audio[1] = new Audio("audio/s8-2.mp3");
	audio[2] = new Audio("audio/award-sound.mp3");
	audio[3] = new Audio("audio/s8-3.mp3");
	
	audio[0].addEventListener("ended", function(){
		audio[1].play();	
	});
	
	nextButton.fadeOut(0);
	checkButton.fadeOut(0);
	result.fadeOut(0);
	award.fadeOut(0);
	snowfall.fadeOut(0);
	
	checkboxes.prop("checked", false);
		
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		audio[0].play();
	};
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
	
	var answerListener = function(){
		$(this).css("font-weight", "bold");
		$(this).siblings().css("font-weight", "");
		if ($(this).attr("data-correct"))
		{
			correctNum ++;
		}
		checkButton.fadeIn(0);
	}
	answers.off("click", answerListener);
	answers.on("click", answerListener);
	
	var checkListener = function()
	{
		for(var i = 0; i < checkboxes.length; i++)
		{
			console.log("checked: " + (typeof $(checkboxes[i]).prop("checked").toString()));
			console.log("attribute: " + (typeof $(checkboxes[i]).attr("data-correct")));
			console.log("result: " + $(checkboxes[i]).prop("checked").toString() === $(checkboxes[i]).attr("data-correct"));
			
			if ($(checkboxes[i]).prop("checked").toString() === $(checkboxes[i]).attr("data-correct"))
			{
				correctNum ++;
			}
		}
		result.append("<br>" + correctNum);
		
		blackSkin.fadeIn(0);
		result.fadeIn(0);
		
		if (correctNum === 7)
		{
			timeout[0] = setTimeout(function(){
				result.fadeOut(0);
				award.fadeIn(0);
				snowfall.fadeIn(0);
				saltContainer.append("<div class='salt'></div>");
				audio[2].play();
				snowfallSprite.play();
			}, 3000);
		}
		
		timeout[1] = setTimeout(function(){
			result.fadeOut(0);
			award.fadeOut(0);
			snowfall.fadeOut(0);
			nextButton.fadeIn(0);
			audio[3].play();
		}, 5000);
	}
	checkButton.off("click", checkListener);
	checkButton.on("click", checkListener);
	
	var checkAnswerListener = function(){
		currElem = $(this);
		currElem.children().prop("checked", true);
	}
	checkAnswer.off("click", checkAnswerListener);
	checkAnswer.on("click", checkAnswerListener);
}

var launch109 = function()
{
	var theFrame = $("#frame-109"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		homeButton = $(prefix + ".home-button"),
		blackSkin = $(prefix + ".black-skin"),
		textContainer = $(prefix + ".text-container"), 
		yunchen = $(prefix + ".s9-yuchen, " + prefix + ".yuchen-label"),
		tuzdybas = $(prefix + ".s9-tuzdybas, " + prefix + ".tuzdybas-label");
	
	var typingText = new TypingText(textContainer, 20, true, function(){
		tuzdybas.fadeIn(500);
		timeout[0] = setTimeout(function(){
			homeButton.fadeIn(0);
		}, 3000);
	});
	
	homeButton.fadeOut(0);
	yunchen.fadeOut(0);
	tuzdybas.fadeOut(0);
	textContainer.fadeOut(0);
		
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		typingText.write();
		textContainer.fadeIn(300);
		timeout[1] = setTimeout(function(){
			yunchen.fadeIn(500);
		}, 5000);
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch301 = function()
{
	var theFrame = $("#frame-301"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		homeButton = $(prefix + ".next-button"),
		nextButton = $(prefix + ".next-task-button"),
		prevButton = $(prefix + ".prev-task-button"),
		checkButton = $(prefix + ".check-button"),
		blackSkin = $(prefix + ".black-skin"),
		result = $(prefix + ".result"),
		award = $(prefix + ".award"),
		snowfall = $(prefix + ".snowfall"),
		answers = $(prefix + ".answer"),
		questionsAnswers = $(prefix + ".question, " + prefix + ".answer"),
		questions1 = $(prefix + ".question-1, " + prefix + ".question-2, " + prefix + ".question-3"),
		questions2 = $(prefix + ".question-4, " + prefix + ".question-5"),
		correctNum = 0;
	
	var snowfallSprite = new Motio(snowfall[0], {
		fps: 3,
		frames: 3
	});
	
	audio[0] = new Audio("audio/award-sound.mp3");
	
	nextButton.fadeOut(0);
	prevButton.fadeOut(0);
	checkButton.fadeOut(0);
	questions2.fadeOut(0);
	result.fadeOut(0);
	award.fadeOut(0);
	snowfall.fadeOut(0);
	homeButton.fadeOut(0);
	
	checkButtonListener = function()
	{
		blackSkin.fadeIn(0);
		result.append(correctNum);
		result.fadeIn(0);
		if (correctNum >= 5)
		{
			timeout[0] = setTimeout(function(){
				result.fadeOut(0);
				award.fadeIn(0);
				snowfall.fadeIn(0);
				snowfallSprite.play();
			}, 3000);
		}
		timeout[0] = setTimeout(function(){
				award.fadeOut(0);
				snowfall.fadeOut(0);
				snowfallSprite.pause();
				homeButton.fadeIn(0);
				result.fadeOut(0);
			}, 5000);
	}
	checkButton.off("click", checkButtonListener);
	checkButton.on("click", checkButtonListener);
	
	var nextButtonListener = function()
	{
		questionsAnswers.fadeOut(0);
		questions2.fadeIn(0);
		nextButton.fadeOut(0);
		prevButton.fadeIn(0);
		checkButton.fadeIn(0);
	}
	nextButton.off("click", nextButtonListener);
	nextButton.on("click", nextButtonListener);
	
	var prevButtonListener = function()
	{
		questionsAnswers.fadeOut(0);
		questions1.fadeIn(0);
		nextButton.fadeIn(0);
		prevButton.fadeOut(0);
		checkButton.fadeOut(0);
	}
	prevButton.off("click", prevButtonListener);
	prevButton.on("click", prevButtonListener);
	
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
	};
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
	
	var answerListener = function(){
		nextButton.fadeIn(0);
		$(this).css("font-weight", "bold");
		if ($(this).attr("data-correct"))
		{
			correctNum ++;
		}
	};
	answers.off("click", answerListener);
	answers.on("click", answerListener);
}

var launch401 = function()
{
	var theFrame = $("#frame-401"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		clams = $(prefix + ".clams"),
		glass2 = $(prefix + ".glass-2"),
		stand = $(prefix + ".stand"),
		spatula = $(prefix + ".spatula"), 
		flameAnimated = $(prefix + ".flame-animated"),
		light = $(prefix + ".light"),
		match = $(prefix + ".match"),
		burner = $(prefix + ".burner"),
		steam = $(prefix + ".steam"),
		scales30 = $(prefix + ".scales-30"),
		scales60 = $(prefix + ".scales-60"),
		resultLabel = $(prefix + ".result-label");
	
	var clamsSprite = new Motio(clams[0], {
		"fps": "3",
		"frames": "3"
	});
	
	var glass2Sprite = new Motio(glass2[0], {
		"fps": "3",
		"frames": "3"
	});
	glass2Sprite.to(2, true);
	
	var standSprite = new Motio(stand[0], {
		"fps": "0.5",
		"frames": "6"
	});
	standSprite.to(1, true);
	standSprite.on("frame", function(){
		if (this.frame === this.frames - 1) {
			this.pause();
			flameAnimated.fadeOut(0);
			steamSprite.pause();
			steam.fadeOut(0);
		}
	});
	
	var steamSprite = new Motio(steam[0], {
		"fps": "3",
		"frames": "2"
	});
	
	audio[0] = new Audio("audio/water-pour.mp3");
	audio[1] = new Audio("audio/match-sound.mp3");
	audio[2] = new Audio("audio/fire-burn.mp3");
	audio[3] = new Audio("audio/glass-sound.mp3");
	audio[4] = new Audio("audio/salt-sound.mp3");
	
	nextButton.fadeOut(0);
	flameAnimated.fadeOut(0);
	light.fadeOut(0);
	steam.fadeOut(0);
	scales60.fadeOut(0);
	scales30.fadeOut(0);
	resultLabel.fadeOut(0);
		
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		blink(glass2, 500, 3);
		timeout[0] = setTimeout(function(){
			blink(stand, 500, 3);	
		},2000);
		timeout[1] = setTimeout(function(){
			audio[0].play();
			standSprite.to(2, true);
			glass2Sprite.to(0, true);
			scales30.fadeIn(0);
		},4000);
		timeout[2] = setTimeout(function(){
			blink(match, 500, 2);
		},6000);
		timeout[3] = setTimeout(function(){
			blink(burner, 500, 2);
		},7000);
		timeout[4] = setTimeout(function(){
			audio[1].play();
			match.fadeOut(0);
			flameAnimated.fadeIn(0);
		},9000);
		timeout[5] = setTimeout(function(){
			steam.fadeIn(0);
			steamSprite.play();
			audio[2].play();
			standSprite.play();
		},10000);
		timeout[6] = setTimeout(function(){
			blink(clams, 500, 2);
		},13000);
		timeout[7] = setTimeout(function(){
			blink(stand, 500, 2);
		},14000);
		timeout[8] = setTimeout(function(){
			audio[3].play();
			clamsSprite.to(1, true);
			standSprite.to(0, true);
		},16000);
		timeout[9] = setTimeout(function(){
			blink(spatula, 500, 2);
		},19000);
		timeout[10] = setTimeout(function(){
			blink(clams, 500, 2);
		},20000);
		timeout[11] = setTimeout(function(){
			blink(glass2, 500, 2);
		},21000);
		timeout[12] = setTimeout(function(){
			audio[4].play();
			clamsSprite.to(2, true);
			glass2Sprite.to(1, true);
			scales30.fadeOut(0);
			scales60.fadeIn(0);
		},22000);
		timeout[13] = setTimeout(function(){
			resultLabel.fadeIn(0);
		},24000);
		timeout[13] = setTimeout(function(){
			resultLabel.addClass("box-shadow-white");
		},25000);
		timeout[13] = setTimeout(function(){
			blackSkin.fadeIn(0);
			nextButton.fadeIn(0);
		},32000);
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var hideEverythingBut = function(elem)
{
	var frames = $(".frame");
	
	frames.fadeOut(0);
	elem.fadeIn(0);
	
	for (var i = 0; i < audio.length; i++)
		audio[i].pause();	

	for (var i = 0; i < timeout.length; i++)
		clearTimeout(timeout[i]);
	
	saltContainer = $(".salt-container");
	saltContainer.fadeOut(0);
	
	switch(elem.attr("id"))
	{
		case "frame-000":
			launch000();
			break;
		case "frame-101":
			launch101();
			break;
		case "frame-102":
			launch102();
			break;
		case "frame-103":
			launch103();
			break;
		case "frame-104":
			launch104();
			break;
		case "frame-105":
			saltContainer.fadeIn(0);
			launch105();
			break;
		case "frame-106":
			saltContainer.fadeIn(0);
			launch106();
			break;
		case "frame-107":
			saltContainer.fadeIn(0);
			launch107();
			break;
		case "frame-108":
			saltContainer.fadeIn(0);
			launch108();
			break;
		case "frame-109":
			saltContainer.fadeIn(0);
			launch109();
			break;
		case "frame-301":
			saltContainer.fadeIn(0);
			launch301();
			break;
		case "frame-401":
			launch401();
			break;
	}
}

var initMenuButtons = function(){
	var links = $(".link");
	links.click(function(){
		var elem = $("#"+$(this).attr("data-link"));
		hideEverythingBut(elem);
	});
};

var main = function()
{
	initMenuButtons();
	//hideEverythingBut($("#pre-load"));
	hideEverythingBut($("#frame-000"));
	//loadImages();
};

$(document).ready(main);